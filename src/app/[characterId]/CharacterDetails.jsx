"use client";

import React from "react";

const CharacterDetails = ({ character }) => {
  if (!character) return <p>Loading...</p>;

  return (
    <>
      <h1 className="text-3xl text-center font-bold my-2">
        Details about {character.name}
      </h1>
      <div className="flex mt-20 justify-center">
        <img src={character.image} alt={character.name} className="rounded-md" />
        <div className="ml-10 text-xl">
          <h1>
            <b>Name: </b> {character.name}
          </h1>
          <p>
            <b>Gender: </b>
            {character.gender}
          </p>
          <p>
            <b>Species: </b> {character.species}
          </p>
          <p>
            <b>Status: </b> {character.status}
          </p>
          <p>
            <b>Location: </b> {character.location.name}
          </p>
        </div>
      </div>
    </>
  );
};

export default CharacterDetails;
