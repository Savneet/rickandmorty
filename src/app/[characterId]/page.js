import { use } from 'react';
import CharacterDetails from './CharacterDetails';
import client from '../graphqlrequest'; 
import { gql } from '@apollo/client';

const particularcharacter = gql`
  query getCharacter($id: ID!) {
    character(id: $id) {
      name
      image
      gender
      species
      status
      location {
        name
      }
    }
  }
`;

async function fetchCharacterData(characterId) {
  const { data } = await client.query({
    query: particularcharacter,
    variables: { id: characterId },
  });
  return data.character;
}

const Page = ({ params }) => {
  const character = use(fetchCharacterData(params.characterId));
  return <CharacterDetails character={character} />;
};

export default Page;
