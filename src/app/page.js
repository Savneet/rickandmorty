import CharacterList from './components/CharacterList';
import { use } from 'react';
import { fetchCharacters } from './fetchCharacters';

export default function Home() {
  const characters = use(fetchCharacters());

  return (
    <div>
      <CharacterList characters={characters} />
    </div>
  );
}