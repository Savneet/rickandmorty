'use client';
import React from 'react'
import { ApolloProvider } from '@apollo/client'
import client from './graphqlrequest'

const ClientProvider = ({children}) => {
  return (
    <ApolloProvider client={client}>
        {children}
    </ApolloProvider>
    // <div>ClientProvider</div>
  )
}

export default ClientProvider