import { gql } from '@apollo/client';
import client from './graphqlrequest';
import { useQuery } from '@apollo/client'; //useQuery hook can only be used in react component . And if I do that, it will start fetching data on the client side which I don't want. 

const charactersQuery = gql`
  query {
    characters {
      results {
        id
        name
        image
        gender
      }
    }
  }
`;

export const fetchCharacters = async () => {
  const { data } = await client.query({
    query: charactersQuery,
  });

  return data.characters.results;
};

// export const fetchCharacters =()=>{
//   const {data, loading,error}= useQuery(charactersQuery);
//   if(loading) return <div>Loading...</div>
//   if(error) return <div>Error</div>
//   return data.characters;
// }