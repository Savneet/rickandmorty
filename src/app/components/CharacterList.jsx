'use client';
import React from 'react'
import { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
const CharacterList = ({characters}) => {

    let [filteredCharacters, setFilteredCharacters]= useState(characters);
    
    const [searchCharacter, setSearchedCharacter] = useState('');

    // const [selectedGender, setSelectedGender]= useState('');

    // setFilteredCharacters(characters);
    
    const handleSearchChange=(event)=>{
      const searchValue=event.target.value;
      setSearchedCharacter(event.target.value);
      console.log(searchCharacter);
      if(searchValue==null || searchValue==undefined)
      {
         setFilteredCharacters(characters);
       
      }
      else{
        setFilteredCharacters(characters.filter((character)=>character.name.toLowerCase().includes(searchValue.toLowerCase())));
      }
        
      //  return setFilteredCharacters(characters.filter((character)=>character.name.toLowerCase().includes(searchCharacter.toLowerCase())));
    }


    const handleGenderFilterChange=(event)=>{

      let selectedFilter= event.target.value;
      setFilteredCharacters(characters.filter((character)=>character.gender.toLowerCase()===selectedFilter.toLowerCase()));
    }



  return (
    // <input type='text' placeholder='Search for a character' onChange={()=>{handleSearchChange}}></input>
    <>
      <h1 className="text-center font-bold text-4xl py-2">Rick and Morty Characters</h1>
      <input type='text' placeholder='Search for a character' onChange={(event)=>{handleSearchChange(event)}} className='p-2 outline-none ml-20'></input>
      <select onChange={(event)=>{handleGenderFilterChange(event)}} className='p-2 outline-none border-none'>
        <option selected disabled >Filter by gender</option>
        <option value='male'>Male</option>
        <option value='female'>Female</option>
        <option value='unknown'>unknown</option>
      </select>
      <div className="grid grid-cols-4 gap-y-10 mx-20 my-10">
        {filteredCharacters.map((character) => (
          <Link key={character.id} href={`/${character.id}`}>
            <div className="hover:cursor-pointer">
              <Image src={character.image} alt={character.name} width={300} height={300} className="rounded-md" />
              <div className="text-xl my-2">Name: {character.name}</div>
              <div className="text-md my-2">Gender: {character.gender}</div>
            </div>
          </Link>
        ))}
      </div>
    </>
  )
}

export default CharacterList